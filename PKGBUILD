# Maintainer: Antoni Aloy Torrens <aaloytorrens@gmail.com>
pkgname=streamlink
pkgver=4.3.0
pkgrel=0
pkgdesc="CLI for extracting streams from various websites to a video player of your choosing"
url="https://streamlink.github.io/"
arch=("any")
license=("BSD-2-Clause")
depends=("python3" "python3-certifi" "python3-isodate" "python3-pycountry" "python3-pycryptodome" "python3-lxml" "python3-socks" "python3-requests" "python3-urllib3" "python3-websocket")
makedepends=("python3-setuptools" "python3-installer")
#"python3-wheel" "python3-gpep517" "python3-versioningit")
checkdepends=("python3-freezegun" "python3-mock" "python3-pytest" "python3-pytest-asyncio" "python3-requests-mock")
source=("$pkgname-$pkgver.tar.gz::https://github.com/streamlink/streamlink/releases/download/$pkgver/streamlink-$pkgver.tar.gz")
options=("!check")
sha256sums=('49ab4f38f11308656e8cd241dff85fcc224bc2ff184e263ddbf6c74243f67d1f')

prepare() {
        # Unexistent packages
        pip install versioningit
        export PATH=$PATH:$HOME/.local/bin
}

build() {
        cd $pkgname-$pkgver

        # Bypass versioningit by setting the default to what we want
        sed -i -E "s|^(default-version =).*$|\1 \"$pkgver\"|" pyproject.toml

        python3 setup.py build
        #PYTHONPATH=$PWD/src/ make -C docs/ man
        #PYTHONPATH=$PWD/build/lib/ bash script/build-shell-completions.sh
}

# python3-freezegun is too old
#check() {
#       cd $pkgname-$pkgver
#       TZ=UTC PYTHONPATH="$builddir/build/lib" python3 -m pytest
#}

package() {
    	cd $pkgname-$pkgver

        python3 setup.py install --root="$pkgdir" --optimize=1 --skip-build

        install -Dm644 docs/_build/man/$pkgname.1 \
                "$pkgdir"/usr/share/man/man1/$pkgname.1

        install -Dm644 completions/bash/$pkgname \
                "$pkgdir"/usr/share/bash-completion/completions/$pkgname
        install -Dm644 completions/zsh/_$pkgname \
                "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}
